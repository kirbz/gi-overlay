# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

IUSE=""
MODS="fixes"
#BASEPOL="2.20120215-r10"
POLICY_FILES="fixes.te fixes.if fixes.fc"

inherit selinux-policy-2

DESCRIPTION="Fixes to the default SELinux policy"

KEYWORDS="~amd64 ~x86"
