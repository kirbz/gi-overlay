# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

IUSE=""
MODS="hw"
#BASEPOL="2.20120215-r10"
POLICY_FILES="hw.te hw.if hw.fc"

inherit selinux-policy-2

DESCRIPTION="SELinux policy for HelloWorld"

KEYWORDS="~amd64 ~x86"
