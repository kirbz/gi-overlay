# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="ConfigControl tool that generates/installs/compares/cleans system config files"
HOMEPAGE="https://gitlab.com/kirbz/gitops"

KEYWORDS="-* ~amd64"
LICENSE="GPL-2"
SLOT="0"

# Runtime dependencies
RDEPEND="
	dev-python/terminaltables
	dev-python/jinja
	dev-python/pyyaml
	gi-repo/gitops-repo
	gi-repo/factory-repo"

# Template from https://forums.gentoo.org/viewtopic-t-1070842.html
case "${PV}" in
	# Head of master branch. This is a Gentoo convention.
	9999)
		EGIT_REPO_URI="https://gitlab.com/kirbz/gitops"
		;;
esac

src_unpack() {
	case "${PV}" in
		9999)
			git-r3_fetch ${EGIT_REPO_URI} ${REFS} ${TAG}
			git-r3_checkout ${EGIT_REPO_URI} "${WORKDIR}/${P}" ${TAG}
			;;
		*)
			default
			;;
	esac
}

src_compile() {
	# We don't want portage to run the Makefile in the factory repo
	return
}

src_install() {
	dobin ConfigControl
}
