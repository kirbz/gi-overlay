# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="Environment definition and config property files"
HOMEPAGE="https://gitlab.com/kirbz/gi-factory"

KEYWORDS="-* ~amd64"
LICENSE="GPL-2"
SLOT="0"

# Runtime dependencies
RDEPEND=""

# We want the entire repo cloned with all branches, not just a shallow commit checkout
#EGIT_CLONE_TYPE="mirror"

# Template from https://forums.gentoo.org/viewtopic-t-1070842.html
case "${PV}" in
	# Head of master branch. This is a Gentoo convention.
	9999)
		EGIT_REPO_URI="https://gitlab.com/kirbz/gi-factory"
		;;
esac

src_unpack() {
	case "${PV}" in
		9999)
			git clone ${EGIT_REPO_URI} ${PF}
			;;
			# The git eclass seems designed for installation, rather than cloning a fully usable
			# local copy of the repository that is checked out on the master branch and ready to use
			# out of the box
			#git-r3_fetch ${EGIT_REPO_URI} ${REFS} ${TAG}
			#git-r3_checkout ${EGIT_REPO_URI} "${WORKDIR}/${P}" ${TAG}
		*)
			default
			;;
	esac
}

src_compile() {
	# We don't want portage to run the Makefile in the factory repo
	return
}

src_install() {
	insinto /var/${PN}
	doins -r * .*
}
